## BIO334 Practical Bioinformatics

## 18-20, May, 2022

Masaomi Hatakeyama
- https://gitlab.uzh.ch/masaomi.hatakeyama/bio334_2022

## How to download/update this documents

Download
```bash
$ git clone https://gitlab.uzh.ch/masaomi.hatakeyama/bio334_2022
```

Updating
```bash
$ git pull
```

Directories
- data/: input data used in some exercises
- examples/: source code of examples shown in the lecture
- jupyter_notebooks/: jupyter notebook files
- skeletons/: incompleted source code of exercises (you should fill in and complete it)
- simulation/: source code used in an advanced exercise of day2

## Table of Content (Plan)

**Day1** | &nbsp; 
-------|-------
13.00- | **Quick Python review** [bio334_day1_part1.ipynb](jupyter_notebooks/bio334_day1_part1.ipynb)
14.00- | **Two sequences comparison** [bio334_day1_part2.ipynb](jupyter_notebooks/bio334_day1_part2.ipynb)
15.00- | **Nucleotide diversity1** [bio334_day1_part3.ipynb](jupyter_notebooks/bio334_day1_part3.ipynb)
 &nbsp;| &nbsp;
**Day2** | &nbsp; 
9.00-12.00 | **Nucleotide diversity2** [bio334_day2_part1.ipynb](jupyter_notebooks/bio334_day2_part1.ipynb) [bio334_day2_part2.ipynb](jupyter_notebooks/bio334_day2_part2.ipynb)
13.00-15.00 | **Tajima's D calculation1** [bio334_day2_part3.ipynb](jupyter_notebooks/bio334_day2_part3.ipynb) [bio334_day2_part4.ipynb](jupyter_notebooks/bio334_day2_part4.ipynb)
15.00-17.00 | **Tajima's D calculation2** [bio334_day2_part5.ipynb](jupyter_notebooks/bio334_day2_part5.ipynb)
 &nbsp;| &nbsp;
**Day3** | &nbsp; 
9.00-12.00 | **Advanced exercise** [bio334_day3.ipynb](jupyter_notebooks/bio334_day3.ipynb)

## Exercise Checker
- https://fgcz-course2.bfabric.org/

## Exercises
- Day1 Part1: https://gist.github.com/masaomi/75ac75aa49d3603697e24864b2345d4d
- Day1 Part2: https://gist.github.com/masaomi/b2f52f4723757d5fd1b93ed422f81923
- Day1 Part3: https://gist.github.com/masaomi/857f8257e4ec9d4fb80557a5890f22a3
- Day2 Part1: https://gist.github.com/masaomi/870f24db57d295eabfd0094b504c2f1b
- Day2 Part2: https://gist.github.com/masaomi/c6c74db5ba67ff58a6b75ac195bd8143
- Day2 Part3: https://gist.github.com/masaomi/c5a11879a9deefccd40c57c1ba975b3a
- Day2 Part4: https://gist.github.com/masaomi/0f51c4a379a0b914c86c84f480e961df
- Day2 Part5: https://gist.github.com/masaomi/29c3325edd802eadb61a213140f33c93
- Day3 Final: https://gist.github.com/masaomi/1397a32c4b870f7ab7e92f479770788d

## Recommended websites

**Python**
- [Python](https://www.python.org/) 
- [Tutorial](https://docs.python.org/3/tutorial/)
- [Glossary](https://docs.python.org/3/glossary.html#glossary)
- [The 5 Best Websites To Learn Python Programming](http://www.makeuseof.com/tag/5-websites-learn-python-programming/)

**General**  
- <http://software-carpentry.org/lessons> and <http://www.datacarpentry.org/lessons>
  Scientific Computing Resources for learning bash shell, programming in python, R, …]  
- [BioStars](https://www.biostars.org/) for questions about biocomputing and scripting for biologists  
- [stackoverflow](http://stackoverflow.com/) for questions related to coding

**Linux/Shell**  
- Software Carpentry tutorial - [The Unix shell](http://swcarpentry.github.io/shell-novice)   
- [Interactive course](http://www.learnshell.org/)  
- [Cheatsheet](https://github.com/swcarpentry/boot-camps/blob/master/shell/shell_cheatsheet.md)  
- [SIB e-learning: UNIX fundamentals](http://edu.isb-sib.ch/pluginfile.php/2878/mod_resource/content/3/couselab-html/content.html)  
- [Explain shell commands](http://explainshell.com/)   
- [Tips & Tricks for using the shell on Mac OS](http://furbo.org/2014/09/03/the-terminal/)  
- [Safe Bash scripting](http://robertmuth.blogspot.ch/2012/08/better-bash-scripting-in-15-minutes.html)

and you may be able to find more better sites by Google...


