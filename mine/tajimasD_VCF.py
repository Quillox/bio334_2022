import argparse
import os
import subprocess

# Command line arguments
parser = argparse.ArgumentParser(description="Calculate Tajima's D from VCF files")
parser.add_argument("--input", "-i", help="Directory containing the .vcf files")
parser.add_argument("--output", "-o", help="Directory to output the results")

def read_vcf(file):
    res = {}
    with open(file) as f:
        for line in f:
            pos = line.split()[2]
            nuc = line.split()[4]
            res[pos] = nuc
    return res

def calc_seg_sites(snps1, snps2, verbose=0):
    """Calculate the number of segregating sites from two SNP data in
    dictionary"""
    if verbose > 0:
        print("Sample1: ", snps1)
        print("Sample2: ", snps2)

    pos1 = set(snps1.keys())
    pos2 = set(snps2.keys())
    common_pos = pos1 & pos2
    exclor_pos = pos1 ^ pos2

    diff = len(exclor_pos)
    for pos in common_pos:
        if not snps1[pos] == snps2[pos]:
            diff += 1
    return diff

def calc_pi(snp_samples, sample_len):
    """
    Calculate pi from a list of SNP dictionaries
    sample_len is the length of the 
    """
    from itertools import combinations

    combs = list(combinations(snp_samples, r=2))
    n_c = len(combs)
    n_n = sample_len
    res = 0.0
    for comb in combs:
        n_seg_sites = 0
        n_seg_sites += calc_seg_sites(comb[0], comb[1])
        res += n_seg_sites/n_n/n_c
    return res

if __name__ == "__main__":
    args = parser.parse_args()

    if not os.path.exists(args.output):
        os.mkdir(args.output)

    samples = []
    for file in os.listdir(path=args.input):
        if file.endswith(".vcf"):
            file_path = os.path.join(args.input, file)
            samples.append(read_vcf(file=file_path))
    
    pi = calc_pi(snp_samples=samples, sample_len=4)
    print(pi)